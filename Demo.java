import java.io.*;

/**
 * Created by User on 28.02.2017.
 */

public class Demo {
    public static void main(String[] args) throws IOException {
        Rational rational1 = new Rational();
        Rational racional2 = new Rational();
        Rational racional3 = new Rational();
        int i = 0;
        String string;

        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\1"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\output.txt"));

        while ((string = bufferedReader.readLine()) != null) {
            i = 0;
            Rational.stringRacional(string);
            String[] strings = string.split(" ");
            try {
                rational1.racionalSplit(strings[0]);
                racional2.racionalSplit(strings[2]);
            } catch (NumberFormatException e) {
                throw new NumberFormatException("ошибка в записи строки");
            }
            switch (strings[1]) {
                case "+":
                    racional3 = rational1.summ(racional2);
                    System.out.println(string + " = " + racional3);
                    break;
                case "-":
                    racional3 = rational1.subtraction(racional2);
                    System.out.println(string + " = " + racional3);
                    break;
                case "*":
                    racional3 = rational1.multiplication(racional2);
                    System.out.println(string + " = " + racional3);
                    break;
                case ":":
                    racional3 = rational1.division(racional2);
                    System.out.println(string + " = " + racional3);
                    break;
                default:
                    System.out.println("Неправильно введены данные");
                    break;
            }
            bufferedWriter.write(String.valueOf(racional3) + "\n");
        }

        bufferedReader.close();
        bufferedWriter.close();
    }
}