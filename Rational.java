import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 03.03.2017.
 */

class Rational {
    private int num;
    private int denum;

    public Rational(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    public Rational() {
        this(1, 1);
    }

    public Rational(int num) {
        this(num, 1);
    }

    public void racionalSplit(String stringRacional) {
        String[] array = new String[3];
        int i = 0;

        for (String splitstring : stringRacional.split("/")) {
            array[i] = splitstring;
            i++;
        }

        this.num = Integer.parseInt(array[0]);
        this.denum = Integer.parseInt(array[1]);

    }

    static boolean stringRacional(String stringRacional) {
        Pattern pattern = Pattern.compile("[-]*[1-9][0-9]*[/]*[-]*[1-9][0-9]*\\s[+:*-]*\\s[-]*[1-9][0-9]*[/]*[-]*[1-9][0-9]");
        Matcher matcher = pattern.matcher(stringRacional);
        return matcher.find();
    }


    public Rational reduction() {
        Rational racional = new Rational();
        int i;
        for (i = 2; i < 9; i++) {
            if (this.num % i == 0 && this.denum % i == 0) {
                racional.num = this.num / i;
                racional.denum = this.denum / i;

            }
        }
        return racional;
    }


    @Override
    public String toString() {
        return +num + "/" + denum;
    }

    public Rational summ(Rational racional2) {
        Rational racional = new Rational();
        racional.num = this.num * racional2.denum + this.denum * racional2.num;
        racional.denum = this.denum * racional2.denum;
        return racional;
    }

    public Rational subtraction(Rational racional2) {
        Rational racional = new Rational();
        racional.num = this.num * racional2.denum - this.denum * racional2.num;
        racional.denum = this.denum * racional2.denum;
        return racional;
    }

    public Rational multiplication(Rational racional2) {
        Rational racional = new Rational();
        racional.num = this.num * racional2.num;
        racional.denum = this.denum * racional2.denum;
        return racional;
    }

    public Rational division(Rational racional2) {
        Rational racional = new Rational();
        racional.num = this.num * racional2.denum;
        racional.denum = this.denum * racional2.num;
        return racional;
    }

}







